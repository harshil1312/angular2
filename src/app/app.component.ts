import {Component, Attribute} from '@angular/core';
import {process, State} from '@progress/kendo-data-query';
import {products} from "./products";

import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
var Ajv = require('ajv');
var ajv = new Ajv({allErrors: true}); // options can be passed, e.g. {allErrors: true}

@Component({
  selector: 'demo',
  styleUrls: ['../../node_modules/@progress/kendo-theme-default/dist/all.css'],
  templateUrl: `./app.component.html`,
})

export class AppComponent {
  validateRes = '';
  validateErrorText = '';
  text = "";
  textJson = {};

  private readSingleFile(e:any) {
    var file = e.target.files[0];
    console.log(file);
    if (!file) {
      // if (!file.type.match('text')) {
      return;
    }
    var reader = new FileReader();
    reader.onload = file => {
      var contents = file.target;
      this.text = contents['result'];
      this.textJson = JSON.parse(contents['result']);
    };
    reader.readAsText(file);
    // console.log(reader.readAsText(file))
  }

  jvf() {
    var validate = ajv.compile(this.textJson[0]);
    var valid = validate(this.textJson[1]);
    if (!valid) console.log(validate.errors);
    this.validateRes = valid;
    this.validateErrorText = JSON.stringify(validate.errors);
  }

  phaseTypeValue:any = {
    0: 'File Archival',
    1: 'File Verification',
    2: 'Record Validation',
    3: 'Transformation',
    4: 'WCIS_ENRICHMENT',
    5: 'DLEA_INVOKED'
  };
  processStatusValue:any = {
    1: 'Started',
    2: 'In Process',
    3: 'Completed'
  };

  // private gridData:any[] = products;
  private state:State = {
    skip: 0,
    take: 5
  };

  private phase:any[] =
    [{
      name: 'Phase 1',
      revision: 151,
      auditType: {},
      ecnId: 876484876956959,
      processStatus: {value: 3},
      auditLog: ['nerrative', 'field name', 'error details'],
      dateTimeCreated: '2017-04-12 11:32:54.765',
      phase: {phsNo: 1}
    }, {
      name: 'Phase 2',
      revision: '',
      auditType: '',
      ecnId: '',
      processStatus: '',
      auditLog: '',
      dateTimeCreated: 1503665607003,
      phase: 'File Verification',
    }, {
      name: 'Phase 3',
      revision: '',
      auditType: '',
      ecnId: '',
      processStatus: '',
      auditLog: '',
      dateTimeCreated: 1503665707003,
      phase: 'Record Validation',
    }, {
      name: 'Phase 4',
      revision: '',
      auditType: '',
      ecnId: '',
      processStatus: '',
      auditLog: '',
      dateTimeCreated: 1503665717003,
      phase: 'Transformation',
    }];

  private gridData:GridDataResult = process(this.phase, this.state);

  protected dataStateChange(state:DataStateChangeEvent):void {
    this.state = state;
    this.gridData = process(this.phase, this.state);
  }
}
