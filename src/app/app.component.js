"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var kendo_data_query_1 = require("@progress/kendo-data-query");
var Ajv = require('ajv');
var ajv = new Ajv({ allErrors: true }); // options can be passed, e.g. {allErrors: true}
var AppComponent = (function () {
    function AppComponent() {
        this.validateRes = '';
        this.validateErrorText = '';
        this.text = "";
        this.textJson = {};
        this.phaseTypeValue = {
            0: 'File Archival',
            1: 'File Verification',
            2: 'Record Validation',
            3: 'Transformation',
            4: 'WCIS_ENRICHMENT',
            5: 'DLEA_INVOKED'
        };
        this.processStatusValue = {
            1: 'Started',
            2: 'In Process',
            3: 'Completed'
        };
        // private gridData:any[] = products;
        this.state = {
            skip: 0,
            take: 5
        };
        this.phase = [{
                name: 'Phase 1',
                revision: 151,
                auditType: {},
                ecnId: 876484876956959,
                processStatus: { value: 3 },
                auditLog: ['nerrative', 'field name', 'error details'],
                dateTimeCreated: '2017-04-12 11:32:54.765',
                phase: { phsNo: 1 }
            }, {
                name: 'Phase 2',
                revision: '',
                auditType: '',
                ecnId: '',
                processStatus: '',
                auditLog: '',
                dateTimeCreated: 1503665607003,
                phase: 'File Verification',
            }, {
                name: 'Phase 3',
                revision: '',
                auditType: '',
                ecnId: '',
                processStatus: '',
                auditLog: '',
                dateTimeCreated: 1503665707003,
                phase: 'Record Validation',
            }, {
                name: 'Phase 4',
                revision: '',
                auditType: '',
                ecnId: '',
                processStatus: '',
                auditLog: '',
                dateTimeCreated: 1503665717003,
                phase: 'Transformation',
            }];
        this.gridData = kendo_data_query_1.process(this.phase, this.state);
    }
    AppComponent.prototype.readSingleFile = function (e) {
        var _this = this;
        var file = e.target.files[0];
        console.log(file);
        if (!file) {
            // if (!file.type.match('text')) {
            return;
        }
        var reader = new FileReader();
        reader.onload = function (file) {
            var contents = file.target;
            _this.text = contents['result'];
            _this.textJson = JSON.parse(contents['result']);
        };
        reader.readAsText(file);
        // console.log(reader.readAsText(file))
    };
    AppComponent.prototype.jvf = function () {
        var validate = ajv.compile(this.textJson[0]);
        var valid = validate(this.textJson[1]);
        if (!valid)
            console.log(validate.errors);
        this.validateRes = valid;
        this.validateErrorText = JSON.stringify(validate.errors);
    };
    AppComponent.prototype.dataStateChange = function (state) {
        this.state = state;
        this.gridData = kendo_data_query_1.process(this.phase, this.state);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'demo',
        styleUrls: ['../../node_modules/@progress/kendo-theme-default/dist/all.css'],
        templateUrl: "./app.component.html",
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map